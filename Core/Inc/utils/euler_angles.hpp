/*
 * euler_angle.hpp
 *
 *  Created on: Oct 11, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_UTILS_EULER_ANGLES_HPP_
#define INC_UTILS_EULER_ANGLES_HPP_

#include <cmath>

#include "utils/quaternion.hpp"

struct EulerAngles
{
	float x;
	float y;
	float z;
};

EulerAngles eulerAnglesFromQuaternion(Quaternion q);


#endif /* INC_UTILS_EULER_ANGLES_HPP_ */
