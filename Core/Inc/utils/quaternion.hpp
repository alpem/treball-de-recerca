/*
 * quaternion.hpp
 *
 *  Created on: 10 d’ag. 2021
 *      Author: alpemwarrior
 */

#ifndef INC_QUATERNION_HPP_
#define INC_QUATERNION_HPP_

class Quaternion
{
public:
	/* Construct blank quaternion (1, 0, 0, 0) */
	Quaternion();
	/* Construct a quaternion from an euler axis and a rotation (in rad) */
	Quaternion(float v1, float v2, float v3, float rot);

	void normalize();
	void scale(float s);
	void transformVectorInv(float vin_x, float vin_y, float vin_z, float* vout_x, float* vout_y, float* vout_z);

	float a; //Scalar part
	float b; //i
	float c; //j
	float d; //k
};

Quaternion operator* (const Quaternion& x, const Quaternion& y);

void slerp(Quaternion q_o, Quaternion q_f, float t, Quaternion* q_out);

#endif /* INC_QUATERNION_HPP_ */
