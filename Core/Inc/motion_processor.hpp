/*
 * motion_processor.hpp
 *
 *  Created on: 3 d’ag. 2021
 *      Author: alpemwarrior
 */

#ifndef INC_MOTION_PROCESSOR_HPP_
#define INC_MOTION_PROCESSOR_HPP_

#include <stdint.h>

#include "sensors/accelerometer.hpp"
#include "sensors/gyroscope.hpp"
#include "sensors/magnetometer.hpp"
#include "utils/quaternion.hpp"
#include "interfaces/job.hpp"
#include "utils/complementary_filter.hpp"
#include "utils/euler_angles.hpp"

#define ACCEL_ORIENTATION_MIN_GRAVITY 9.5
#define ACCEL_ORIENTATION_MAX_GRAVITY 10.2

class MotionProcessor : public Job
{
public:
	/* As a base there only really needs to be a gyroscope and accelerometer.
	 * Other sensors can be added as well (Barometers, GPS sensors, magnetometers, etc).
	 */
	void init(job_period frequency, Accelerometer* accel, Gyroscope* gyro, Magnetometer* mag = nullptr);
	void loop();

	void jobLoop();

	float var1;
	float var2;
	float var3;

	Quaternion angular_position_quat;
	EulerAngles angular_position_ea;

private:
	Accelerometer* accel;
	Gyroscope* gyro;
	Magnetometer* mag;

	ComplementaryFilter cf;

	uint8_t period;
};



#endif /* INC_MOTION_PROCESSOR_HPP_ */
