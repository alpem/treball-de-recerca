/*
 * stabilized_mode.hpp
 *
 *  Created on: Oct 6, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_MODES_STABILIZED_MODE_HPP_
#define INC_MODES_STABILIZED_MODE_HPP_

#include <utils/pid_controller_angle.hpp>
#include "interfaces/mode.hpp"

class StabilizedMode : public Mode
{
public:
	StabilizedMode();

	void resetMode();
	void updateMode();

private:
	float yaw_setpoint;

	EulerAngles orientation;

	float out_x, out_y, out_z;

	MotionProcessor* mp;

	ControllerPIDAngle pid_x;
	ControllerPIDAngle pid_y;
	ControllerPIDAngle pid_z;
};

#endif /* INC_MODES_STABILIZED_MODE_HPP_ */
