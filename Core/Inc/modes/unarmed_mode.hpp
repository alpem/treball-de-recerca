/*
 * unarmed_mode.hpp
 *
 *  Created on: Sep 21, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_MODES_UNARMED_MODE_HPP_
#define INC_MODES_UNARMED_MODE_HPP_

#include "interfaces/mode.hpp"

class UnarmedMode : public Mode
{
public:
	void resetMode();
	void updateMode();
};



#endif /* INC_MODES_UNARMED_MODE_HPP_ */
