/*
 * config_manager.hpp
 *
 *  Created on: Aug 1, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_CONFIG_MANAGER_HPP_
#define INC_CONFIG_MANAGER_HPP_

#include <stdint.h>
#include "fatfs.h"

typedef enum
{
	CONFIG_OK,
	CONFIG_FILE_ERROR,
	CONFIG_NO_FIELD,
	CONFIG_FORMAT_ERROR,
	CONFIG_MISC_ERROR
} config_return_t;

typedef struct
{
	const char* key;
	char value[30];
	signed char lineno;
} value_pair;

class ConfigManager
{
public:
	config_return_t getValue(const char* key, float* value);
	config_return_t getValue(const char* key, int32_t* value);
	config_return_t getValue(const char* key, char* value, uint8_t size);

	config_return_t findValue(const char* key, unsigned char * lineno);

	void init(FIL* file_handle);
private:
	static int INIHandler(void* user, const char* section, const char* name, const char* value, int lineno);
	FIL* file_handle;
};



#endif /* INC_CONFIG_MANAGER_HPP_ */
