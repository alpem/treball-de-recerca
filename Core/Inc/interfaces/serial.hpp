/*
 * serial.hpp
 *
 *  Created on: Jul 6, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_SERIAL_H_
#define INC_SERIAL_H_

#include <stdint.h>

class Serial
{
public:
	virtual void receive(uint8_t data) = 0;
};

#endif /* INC_SERIAL_H_ */
