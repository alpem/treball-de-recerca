/*
 * rx.hpp
 *
 *  Created on: 10 de jul. 2021
 *      Author: alpemwarrior
 */

#ifndef INC_RX_HPP_
#define INC_RX_HPP_

#include "config/hal_conf.h"

class Rx
{
public:
	uint16_t rx_channels[HAL_RX_CHANNELS];
};

#endif /* INC_RX_HPP_ */
