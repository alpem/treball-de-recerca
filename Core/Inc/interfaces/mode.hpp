/*
 * mode.hpp
 *
 *  Created on: Sep 18, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_INTERFACES_MODE_HPP_
#define INC_INTERFACES_MODE_HPP_

#include "output_manager.hpp"
#include "motion_processor.hpp"
#include "interfaces/rx.hpp"

class Mode
{
public:
	Mode();
	virtual void resetMode() = 0;
	virtual void updateMode() = 0;
protected:
	MotionProcessor* mp;
	OutputManager* out;
	Rx *rx;
};



#endif /* INC_INTERFACES_MODE_HPP_ */
