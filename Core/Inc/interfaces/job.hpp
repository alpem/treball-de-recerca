/*
 * job.hpp
 *
 *  Created on: Sep 7, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_INTERFACES_JOB_HPP_
#define INC_INTERFACES_JOB_HPP_

#include "config/hal_conf.h"

typedef enum
{
	JOB_FREQ_10S = 10000,
	JOB_FREQ_20S = 20000,
	JOB_FREQ_5S = 5000,
	JOB_FREQ_1HZ  = 1000,
	JOB_FREQ_25HZ = 40,
	JOB_FREQ_50HZ = 20,
	JOB_FREQ_100HZ = 10,
	JOB_FREQ_200HZ = 5,
	JOB_FREQ_500HZ = 2,
	JOB_FREQ_1000HZ	= 1
}job_period;

class Job
{
public:
	void jobRegister(job_period frequency);
	virtual void jobLoop() = 0;

	unsigned long last_execution;
#if HAL_TASK_MANAGER_USE_LAST_EXECUTION_TIME
	unsigned long last_execution_time;
#endif
	job_period frequency;
};



#endif /* INC_INTERFACES_JOB_HPP_ */
