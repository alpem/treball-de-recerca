/*
 * debug_utils.hpp
 *
 *  Created on: Jul 28, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_DEBUG_UTILS_HPP_
#define INC_DEBUG_UTILS_HPP_

#include "usart.h"

void printDebugFloat(UART_HandleTypeDef* handle, float decimal_number);
//void printDebug(UART_HandleTypeDef* handle, int32_t integer_number);
void printDebugString(UART_HandleTypeDef* handle, uint8_t* buffer, uint8_t size);

#endif /* INC_DEBUG_UTILS_HPP_ */
