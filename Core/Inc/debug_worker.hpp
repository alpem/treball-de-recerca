/*
 * debug_worker.hpp
 *
 *  Created on: 8 de set. 2021
 *      Author: alpemwarrior
 */

#ifndef INC_DEBUG_WORKER_HPP_
#define INC_DEBUG_WORKER_HPP_

#include "interfaces/job.hpp"
#include "usart.h"
#include "hal.hpp"

class DebugWorker : public Job
{
public:
	DebugWorker(UART_HandleTypeDef *timer, job_period frequecy);
	void jobLoop();

private:
	char buffer[200];
	int bytes;
	bool lol;

	UART_HandleTypeDef *handle;
	void printDebugString();
	//void printDebugFloat(UART_HandleTypeDef* handle, float decimal_number);
};



#endif /* INC_DEBUG_WORKER_HPP_ */
