/*
 * serial_manager.hpp
 *
 *  Created on: 10 de jul. 2021
 *      Author: alpemwarrior
 */

#ifndef INC_SERIAL_MANAGER_HPP_
#define INC_SERIAL_MANAGER_HPP_

#include "interfaces/serial.hpp"

#define SERIAL_MAX_CHANNELS 2

extern "C"{ void serial_receive(uint8_t data, uint8_t index); }

void serial_register_listener(Serial* handle, uint8_t index);

#endif /* INC_SERIAL_MANAGER_HPP_ */
