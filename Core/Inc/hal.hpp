/*
 * hal.hpp
 *
 *  Created on: 3 de jul. 2021
 *      Author: alpemwarrior
 */

#ifndef INC_HAL_H_
#define INC_HAL_H_

/* Includes for all modules */
#include "storage_manager.hpp"
#include "config_manager.hpp"
#include "log_manager.hpp"
#include "output_manager.hpp"
#include "motion_processor.hpp"
#include "serial_manager.hpp"
#include "task_manager.hpp"
#include "interfaces/rx.hpp"
#include "interfaces/servo.hpp"
#include "motion_processor.hpp"
#include "state_tracker.hpp"

/* Include configuration file */
#include "config/hal_conf.h"

class HAL
{
public:
	static HAL& getInstance();

	void init();

	Rx*	    getReceiver();

	Accelerometer* getAccelerometer();
	Gyroscope* 	   getGyroscope();
	Magnetometer*  getMagnetometer();

	TaskManager*    getTaskManager();
	MotionProcessor* getMotionProcessor();
	StorageManager* getStorageManager();
	OutputManager* getOutputManager();

	void softFaultHandler();

	HAL(HAL const&) = delete;
	void operator=(HAL const&) = delete;
private:
	HAL(){}

	Accelerometer* accel;
	Gyroscope* 	   gyro;
	Magnetometer*  mag;

	Rx       *receiver;

	OutputManager output_manager;

	MotionProcessor motion_processor;

	TaskManager task_manager;

	StateTracker state_tracker;

#if HAL_USE_STORAGE_MANAGER
	StorageManager storage_manager;
#endif
};

#endif /* INC_HAL_H_ */
