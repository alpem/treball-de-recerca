/*
 * accelerometer.hpp
 *
 * Describes an interface for all objects implementing an accelerometer sensor
 *
 *  Created on: Jul 25, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_ACCELEROMETER_HPP_
#define INC_ACCELEROMETER_HPP_

class Accelerometer
{
public:
	virtual void accelCalibrate()  = 0;

	//Output in m / s2
	float accel_x;
	float accel_y;
	float accel_z;
};


#endif /* INC_ACCELEROMETER_HPP_ */
