/*
 * gyroscope.hpp
 *
 * Describes an interface for all objects implementing a gyroscope sensor.
 *
 *  Created on: Jul 25, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_GYROSCOPE_HPP_
#define INC_GYROSCOPE_HPP_

class Gyroscope
{
public:
	virtual void gyroCalibrate()  = 0;

	//Measurement in degrees per second.
	float gyro_x;
	float gyro_y;
	float gyro_z;
};

#endif /* INC_GYROSCOPE_HPP_ */
