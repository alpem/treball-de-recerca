/*
 * state_tracker.hpp
 *
 *  Created on: Sep 18, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_STATE_TRACKER_HPP_
#define INC_STATE_TRACKER_HPP_

#include <stdint.h>

#include "interfaces/rx.hpp"
#include "output_manager.hpp"
#include "interfaces/job.hpp"
#include "interfaces/mode.hpp"

#define STATE_TRACKER_SEGMENT_HYSTERESIS 20

class StateTracker : public Job
{
public:
	void init(job_period frequency);
	void jobLoop();

private:
	void parseAuxChannels();
	bool auxParser(int16_t value, int8_t segments, int16_t *out);

	Rx *receiver;
	OutputManager* output;

	Mode *unarmed_mode;
	Mode *mode_list[2];
	Mode *mode_pointer;

	/* State variables */
	bool    armed;
    uint8_t mode_num;
};

#endif /* INC_STATE_TRACKER_HPP_ */
