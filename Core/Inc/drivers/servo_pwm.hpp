/*
 * servo_pwm.h
 *
 *  Created on: Jul 9, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_SERVO_PWM_HPP_
#define INC_SERVO_PWM_HPP_

#include <tim.h>

#include "config/hal_conf.h"
#include "interfaces/servo.hpp"

#define SERVO_PWM_MIN 45   //Minimum duty cycle, in %1000
#define SERVO_PWM_RANGE 45 //Variation of duty cycle, in %1000

class ServoPWM : public Servo
{
public:
	ServoPWM(TIM_HandleTypeDef* timer, uint32_t channel, bool inverted);
	void update(uint16_t value);

private:
	bool inverted;
	TIM_HandleTypeDef* timer;
	uint32_t channel;
	uint16_t* data_pointer;
};

#endif /* INC_SERVO_PWM_HPP_ */
