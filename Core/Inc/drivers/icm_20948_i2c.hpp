/*
 * icm_20948_i2c.hpp
 *
 * This file uses code from the following library: https://github.com/adafruit/Adafruit_ICM20X, which is licensed under the BSD
 * license (see Licenses/BSD_Adafruit.txt)
 *
 *  Created on: 26 de jul. 2021
 *      Author: alpemwarrior
 */

#ifndef INC_ICM_20948_I2C_HPP_
#define INC_ICM_20948_I2C_HPP_

#include <stdint.h>
#include "i2c.h"
#include "interfaces/job.hpp"
#include "sensors/accelerometer.hpp"
#include "sensors/gyroscope.hpp"
#include "sensors/magnetometer.hpp"

#define I2C_MASTER_RESETS_BEFORE_FAIL 5 // Misc configuration macros
#define NUM_FINISHED_CHECKS 100 // How many times to poll I2C_SLV4_DONE before giving up and resetting

#define ACCEL_BIAS_X_KEY "acc_b_x"
#define ACCEL_BIAS_Y_KEY "acc_b_y"
#define ACCEL_BIAS_Z_KEY "acc_b_z"

#define GYRO_BIAS_X_KEY  "gyr_b_x"
#define GYRO_BIAS_Y_KEY  "gyr_b_y"
#define GYRO_BIAS_Z_KEY  "gyr_b_z"

#define MAG_BIAS_X_KEY   "mag_b_x"
#define MAG_BIAS_Y_KEY   "mag_b_y"
#define MAG_BIAS_Z_KEY   "mag_b_z"

#define ICM20948_ADDRESS_1 0x69
#define ICM20948_ADDRESS_2 0x68

#define ICM20948_MAG_ID 0x09          ///< The chip ID for the magnetometer

#define ICM20948_UT_PER_LSB 0.15 ///< mag data LSB value (fixed)

#define AK09916_WIA2 0x01  ///< Magnetometer
#define AK09916_ST1 0x10   ///< Magnetometer
#define AK09916_HXL 0x11   ///< Magnetometer
#define AK09916_HXH 0x12   ///< Magnetometer
#define AK09916_HYL 0x13   ///< Magnetometer
#define AK09916_HYH 0x14   ///< Magnetometer
#define AK09916_HZL 0x15   ///< Magnetometer
#define AK09916_HZH 0x16   ///< Magnetometer
#define AK09916_ST2 0x18   ///< Magnetometer
#define AK09916_CNTL2 0x31 ///< Magnetometer
#define AK09916_CNTL3 0x32 ///< Magnetometer

#define ICM20X_B0_WHOAMI 0x00         ///< Chip ID register
#define ICM20X_B0_USER_CTRL 0x03      ///< User Control Reg. Includes I2C Master
#define ICM20X_B0_LP_CONFIG 0x05      ///< Low Power config
#define ICM20X_B0_REG_INT_PIN_CFG 0xF ///< Interrupt config register
#define ICM20X_B0_REG_INT_ENABLE 0x10 ///< Interrupt enable register 0
#define ICM20X_B0_REG_INT_ENABLE_1 0x11 ///< Interrupt enable register 1
#define ICM20X_B0_I2C_MST_STATUS 0x17 ///< Records if I2C master bus data is finished

#define ICM20X_B0_REG_BANK_SEL 0x7F ///< register bank selection register
#define ICM20X_B0_PWR_MGMT_1 0x06   ///< primary power management register
#define ICM20X_B0_ACCEL_XOUT_H 0x2D ///< first byte of accel data
#define ICM20X_B0_GYRO_XOUT_H 0x33  ///< first byte of gyro data

// Bank 2
#define ICM20X_B2_GYRO_SMPLRT_DIV 0x00    ///< Gyroscope data rate divisor
#define ICM20X_B2_GYRO_CONFIG_1 0x01      ///< Gyro config for range setting
#define ICM20X_B2_ACCEL_SMPLRT_DIV_1 0x10 ///< Accel data rate divisor MSByte
#define ICM20X_B2_ACCEL_SMPLRT_DIV_2 0x11 ///< Accel data rate divisor LSByte
#define ICM20X_B2_ACCEL_CONFIG_1 0x14     ///< Accel config for setting range

// Bank 3
#define ICM20X_B3_I2C_MST_ODR_CONFIG 0x0 ///< Sets ODR for I2C master bus
#define ICM20X_B3_I2C_MST_CTRL 0x1       ///< I2C master bus config
#define ICM20X_B3_I2C_MST_DELAY_CTRL 0x2 ///< I2C master bus config
#define ICM20X_B3_I2C_SLV0_ADDR                                                \
  0x3 ///< Sets I2C address for I2C master bus slave 0
#define ICM20X_B3_I2C_SLV0_REG                                                 \
  0x4 ///< Sets register address for I2C master bus slave 0
#define ICM20X_B3_I2C_SLV0_CTRL 0x5 ///< Controls for I2C master bus slave 0
#define ICM20X_B3_I2C_SLV0_DO 0x6   ///< Sets I2C master bus slave 0 data out

#define ICM20X_B3_I2C_SLV4_ADDR                                                \
  0x13 ///< Sets I2C address for I2C master bus slave 4
#define ICM20X_B3_I2C_SLV4_REG                                                 \
  0x14 ///< Sets register address for I2C master bus slave 4
#define ICM20X_B3_I2C_SLV4_CTRL 0x15 ///< Controls for I2C master bus slave 4
#define ICM20X_B3_I2C_SLV4_DO 0x16   ///< Sets I2C master bus slave 4 data out
#define ICM20X_B3_I2C_SLV4_DI 0x17   ///< Sets I2C master bus slave 4 data in
#define ICM20X_EXT_SLV_SENS_DATA_01 60
#define ICM20948_CHIP_ID 0xEA ///< ICM20948 default device id from WHOAMI
#define ICM20649_CHIP_ID 0xE1 ///< ICM20649 default device id from WHOAMI

#define STANDARD_GRAVITY 9.80991

#define ICM_20948_I2C_TIMEOUT 10 //In ms

typedef enum {
  ICM20948_ACCEL_RANGE_2_G = 0x00,
  ICM20948_ACCEL_RANGE_4_G,
  ICM20948_ACCEL_RANGE_8_G,
  ICM20948_ACCEL_RANGE_16_G,

} icm20948_accel_range_t;

typedef enum {
  ICM20948_GYRO_RANGE_250_DPS = 0x00,
  ICM20948_GYRO_RANGE_500_DPS,
  ICM20948_GYRO_RANGE_1000_DPS,
  ICM20948_GYRO_RANGE_2000_DPS,

} icm20948_gyro_range_t;

typedef enum {
  AK09916_MAG_DATARATE_SHUTDOWN = 0x0, ///< Stops measurement updates
  AK09916_MAG_DATARATE_SINGLE = 0x1, ///< Takes a single measurement then switches to AK09916_MAG_DATARATE_SHUTDOWN
  AK09916_MAG_DATARATE_10_HZ = 0x2,  ///< updates at 10Hz
  AK09916_MAG_DATARATE_20_HZ = 0x4,  ///< updates at 20Hz
  AK09916_MAG_DATARATE_50_HZ = 0x6,  ///< updates at 50Hz
  AK09916_MAG_DATARATE_100_HZ = 0x8, ///< updates at 100Hz
} ak09916_data_rate_t;

class ICM_20948_I2C : public Gyroscope, public Accelerometer, public Magnetometer, public Job
{
public:
	ICM_20948_I2C(job_period freq, I2C_HandleTypeDef* i2c_handle, bool alternative_address);
	void init();

	void accelCalibrate();
	void gyroCalibrate();
	void magCalibrate();

	void jobLoop();
private:
	I2C_HandleTypeDef* i2c_handle;

	void setBank(uint8_t bank_number);							   //Switch banks
	void writeBit(uint8_t register_number, uint8_t bit, bool set); //Helper function to read the state of a register and change only a specific bit.
	void writeRegister(uint8_t register_number, uint8_t value);    //Helper function to set the state of a register to a certain value.
	uint8_t readRegister(uint8_t register_number);

	void reset();			//Reset the device
	void disableSleep();    //Get device out of the default sleep mode.

	void setI2CBypass(bool state);
	void configureI2CMaster();
	void enableI2CMaster(bool state);
	bool checkForMagConnection();
	void resetI2CMaster();
	void setUpMagProxy();

	void    writeMagRegister(uint8_t register_address, uint8_t value);
	uint8_t readMagRegister(uint8_t register_address);
	uint8_t externalTransaction(uint8_t slave_address, uint8_t register_address, uint8_t value, bool read);

	uint8_t magReadId();
	void setGyroRange(icm20948_gyro_range_t range);
	void setAccelRange(icm20948_accel_range_t range);

	void setGyroRateDivider(uint16_t new_divider);
	void setAccelRateDivider(uint16_t new_divider);

	void setMagDataRate(ak09916_data_rate_t data_rate);

	uint8_t readId();

	void faultHandler();

	uint8_t address;
	int8_t selected_bank = -1;

	icm20948_accel_range_t accel_range;
	icm20948_gyro_range_t  gyro_range;

	HAL_StatusTypeDef return_code;

	float accel_offset_x;
	float accel_offset_y;
	float accel_offset_z;


	float gyro_offset_x;
	float gyro_offset_y;
	float gyro_offset_z;

	float mag_offset_x;
	float mag_offset_y;
	float mag_offset_z;

	int16_t raw_accel_x;
	int16_t raw_accel_y;
	int16_t raw_accel_z;

	int16_t raw_gyro_x;
	int16_t raw_gyro_y;
	int16_t raw_gyro_z;

	int16_t raw_mag_x;
	int16_t raw_mag_y;
	int16_t raw_mag_z;
};

#endif /* INC_ICM_20948_I2C_HPP_ */
