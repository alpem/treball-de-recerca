/*
 * blackbox.hpp
 *
 *  Created on: 29 de set. 2021
 *      Author: alpemwarrior
 */

#ifndef INC_BLACKBOX_HPP_
#define INC_BLACKBOX_HPP_

#include "fatfs.h"

#include "interfaces/job.hpp"

#include "sensors/gyroscope.hpp"
#include "sensors/magnetometer.hpp"
#include "sensors/accelerometer.hpp"

#include "motion_processor.hpp"
#include "output_manager.hpp"

class Blackbox : public Job
{
public:
	void jobLoop();
	void init(FIL* file_handle, job_period freq);

private:
	void printString();

	uint8_t buffer[300];
	int bytes;
	unsigned int rec_bytes;
	unsigned long time;
	FRESULT fres;

	FIL* file_handle;

	Gyroscope* gyro;
	Magnetometer* mag;
	Accelerometer* accel;

	MotionProcessor* mp;
	OutputManager*   out;
};



#endif /* INC_BLACKBOX_HPP_ */
