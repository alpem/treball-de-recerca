/*
 * log_manager_wrapper.hpp
 *
 *  Created on: Oct 3, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_LOG_MANAGER_WRAPPER_HPP_
#define INC_LOG_MANAGER_WRAPPER_HPP_

#include "hal.hpp"

void writeLog(const char* string);

#endif /* INC_LOG_MANAGER_WRAPPER_HPP_ */
