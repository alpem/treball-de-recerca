/*
 * task_manager.hpp
 *
 *  Created on: Sep 7, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_TASK_MANAGER_HPP_
#define INC_TASK_MANAGER_HPP_

#include "interfaces/job.hpp"
#include "config/hal_conf.h"
#include <stdint.h>

class TaskManager
{
public:
	void init();
	void loop();
	void registerTask(Job* job);

	Job *job_list[HAL_TASK_MANAGER_MAX_JOBS];

	uint8_t index = 0;
};

#endif /* INC_TASK_MANAGER_HPP_ */
