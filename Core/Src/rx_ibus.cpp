/*
 * rx_ibus2.cpp
 *
 *  Created on: 10 de jul. 2021
 *      Author: alpemwarrior
 */

#include "drivers/rx_ibus.hpp"
#include "log_manager_wrapper.hpp"

RxIBUS::RxIBUS(int serial_index)
{
	serial_register_listener(this, serial_index);
	jobRegister(JOB_FREQ_500HZ);
	index = 0;
#if RX_IBUS_DEBUG_FRAMERATE
	last_frame = HAL_GetTick();
#endif
	changed = false;
	return;
	writeLog("Initialized IBUS receiver");
}

void RxIBUS::jobLoop()
{
	update();
}

void RxIBUS::receive(uint8_t new_data)
{
	buffer[index] = new_data;
	int first = index - 31;
	if (first < 0) first = BUFFER_SIZE + first;

	//Only calculate checksums for valid packet sizes
	if (buffer[first] == 0x20)
	{
		if (checkFrame(0x20, first))
		{
			index = 0;
#if RX_IBUS_DEBUG_FRAMERATE
			updateFramerate();
#endif
			saveChannelData(first);
			return;
		}
	}

	if (index < BUFFER_SIZE - 1)
	{
		index++;
	}
	else
	{
		index = 0;
	}

	return;
}

void RxIBUS::update()
{
	//Avoid copy if the private buffer hasn't changed
	if (!changed) { return; }

	//Copy internal buffer to public buffer
	for (int i = 0; i < RX_IBUS_CHANNELS; i++)
	{
		rx_channels[i] = (channels[i] - 1000) * 2;
	}

	changed = false;
	return;
}

void RxIBUS::saveChannelData(uint8_t head)
{
	for (int i = head; i < RX_IBUS_CHANNELS; i++)
	{
		channels[i] = buffer[i * 2 + 2 + head] + (buffer[i * 2 + head + 3] << 8);
	}
	changed = true;
	return;
}

bool RxIBUS::checkFrame(uint8_t data_size, uint8_t first)
{
	int8_t index = first;
	uint32_t calculated_sum = 0;

	//The checksum bytes are the two last bytes, with the least significant byte first
	signed int checksum_msb_index = first + data_size - 1;
	signed int checksum_lsb_index = first + data_size - 2;
	if (checksum_lsb_index >= BUFFER_SIZE) checksum_lsb_index -= BUFFER_SIZE;
	if (checksum_msb_index >= BUFFER_SIZE) checksum_msb_index -= BUFFER_SIZE;
	uint32_t checksum = buffer[checksum_lsb_index] + (buffer[checksum_msb_index] << 8);

	for (uint8_t i = 0; i < data_size - 2; i++)
	{
		if (index == BUFFER_SIZE)
		{
			index = 0;
		}
		calculated_sum += buffer[index];
		index++;
	}

	return (calculated_sum + checksum) == 0xFFFF;
}

#if RX_IBUS_DEBUG_FRAMERATE
void RxIBUS::updateFramerate()
{
	frametime = HAL_GetTick() - last_frame;
	last_frame = HAL_GetTick();
	if (frametime > 0) {
		framerate = 1000 / frametime;
	};
	return;
}
#endif
