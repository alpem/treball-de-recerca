/*
 * Serial.cpp
 *
 *  Created on: 10 de jul. 2021
 *      Author: alpemwarrior
 */

#include "serial_manager.hpp"
#include "log_manager_wrapper.hpp"
#include "usart.h"

static Serial* listeners[SERIAL_MAX_CHANNELS];

void serial_receive(uint8_t data, uint8_t index)
{
	if (listeners[index])
	{
		listeners[index]->receive(data);
	}
	return;
}

void serial_register_listener(Serial* handle, uint8_t index)
{
	listeners[index] = handle;
	__HAL_UART_ENABLE_IT(&huart1, UART_IT_RXNE);
	writeLog("Registered serial listener");
	return;
}
