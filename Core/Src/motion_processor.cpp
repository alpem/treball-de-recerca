/*
 * motion_processor.cpp
 *
 *  Created on: 10 d’ag. 2021
 *      Author: alpemwarrior
 */

#include "motion_processor.hpp"
#include <cmath>
#include <limits>

void MotionProcessor::init(job_period frequency, Accelerometer* accel, Gyroscope* gyro, Magnetometer* mag /*= nullptr*/)
{
	this->accel = accel;
	this->gyro = gyro;
	this->mag = mag;
	period = frequency;
	jobRegister((job_period)period);
	cf.init(period / 1000.0f, 0.7);
	return;
}

void MotionProcessor::jobLoop()
{
	loop();
}

void MotionProcessor::loop()
{
	cf.loop(&gyro->gyro_x, &gyro->gyro_y, &gyro->gyro_z, &accel->accel_x, &accel->accel_y, &accel->accel_z, &mag->mag_x, &mag->mag_y, &mag->mag_z);
	angular_position_quat = cf.orientation;
	angular_position_quat.normalize();
	angular_position_ea = eulerAnglesFromQuaternion(angular_position_quat);
	var1 = cf.var1;
	var2 = cf.var2;
	var3 = cf.var3;
}
