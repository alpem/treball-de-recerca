/*
 * pid_controller.cpp
 *
 *  Created on: 6 d’oct. 2021
 *      Author: alpemwarrior
 */

#include <utils/pid_controller_angle.hpp>
#include "tim.h"

#define M_PI 3.14159265358979323846

ControllerPIDAngle::ControllerPIDAngle()
{
	reset();
	setParameters(0.0, 0.0, 0.0);
	setSetpoint(0.0);
}

void ControllerPIDAngle::setParameters(float proportional, float integral, float derivative)
{
	p = proportional;
	i = integral;
	d = derivative;
}

void ControllerPIDAngle::setSetpoint(float setpoint)
{
	this->setpoint = setpoint;
}

void ControllerPIDAngle::reset()
{
	integral_sum = 0.0;
	last_error = 0.0;
	last_time = HAL_GetTick();
}

float ControllerPIDAngle::loop(float measurement)
{
	float output;
	float error = setpoint - measurement;
	if (error < (-M_PI))
	{
		error = 2 * M_PI + error;
	}
	else if (error > M_PI)
	{
		error = error - (2 * M_PI);
	}
	float period = (HAL_GetTick() - last_time) / 1000;

	integral_sum += ((last_error + error) / 2.0f) * period;

	output = p * (error) + i * integral_sum + d * ((error - last_error) / period);

	last_error = error;
	last_time = HAL_GetTick();

	return output;
}
