/*
 * job.cpp
 *
 *  Created on: Sep 7, 2021
 *      Author: alpemwarrior
 */

#include "interfaces/job.hpp"
#include "hal.hpp"

void Job::jobRegister(job_period frequency)
{
	this->last_execution = 0;
	this->frequency = frequency;
	HAL::getInstance().getTaskManager()->registerTask(this);
	return;
}


