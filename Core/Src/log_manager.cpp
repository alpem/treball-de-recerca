/*
 * log_manager.cpp
 *
 *  Created on: 2 d’ag. 2021
 *      Author: alpemwarrior
 */

#include "log_manager.hpp"

void LogManager::init(FIL* file_handle)
{
	this->file_handle = file_handle;
}

void LogManager::print(const char* message)
{
    f_printf(file_handle, "%s\n", message);
}


