/*
 * debug_utils.cpp
 *
 *  Created on: Jul 28, 2021
 *      Author: alpemwarrior
 */

#include "debug_utils.hpp"
#include <stdio.h>

void printDebugFloat(UART_HandleTypeDef* handle, float decimal_number)
{
	uint8_t buffer[64];
	int8_t size = snprintf((char*)buffer, 64, "%f", decimal_number);
	if (size < 0) {return;}
	buffer[size] = '\n';
    HAL_UART_Transmit(handle, buffer, size + 1, HAL_MAX_DELAY);
	return;
}

void printDebugString(UART_HandleTypeDef* handle, uint8_t* buffer, uint8_t size)
{
    HAL_UART_Transmit(handle, buffer, size, HAL_MAX_DELAY);
	return;
}



