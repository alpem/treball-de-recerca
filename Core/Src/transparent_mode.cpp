/*
 * transparent_mode.cpp
 *
 *  Created on: Sep 18, 2021
 *      Author: alpemwarrior
 */

#include "modes/transparent_mode.hpp"

void TransparentMode::updateMode()
{
	out->set(0, rx->rx_channels[0]);
	out->set(1, rx->rx_channels[1]);
	out->set(2, rx->rx_channels[2]);
	out->set(3, rx->rx_channels[3]);
}

void TransparentMode::resetMode()
{

}

