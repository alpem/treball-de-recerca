/*
 * state_tracker.cpp
 *
 *  Created on: Sep 18, 2021
 *      Author: alpemwarrior
 */

#include <stdio.h>

#include "state_tracker.hpp"

#include "config/hal_conf.h"
#include "log_manager_wrapper.hpp"
#include "modes/transparent_mode.hpp"
#include "modes/unarmed_mode.hpp"
#include "modes/stabilized_mode.hpp"

void StateTracker::init(job_period frequency)
{
	jobRegister(frequency);

	mode_num = 0;
	armed = false;

	receiver   = HAL::getInstance().getReceiver();
	output = HAL::getInstance().getOutputManager();

	static UnarmedMode um = UnarmedMode();
	unarmed_mode = &um;
	static TransparentMode mode1 = TransparentMode();
	static StabilizedMode  mode2 = StabilizedMode();

	mode_list[0] = &mode1;
	mode_list[1] = &mode2;

	mode_pointer = nullptr;

	writeLog("Initialized StateTracker");
}

void StateTracker::jobLoop()
{
	parseAuxChannels();

	if (!armed)
	{
		unarmed_mode->updateMode();
		mode_pointer = nullptr;
		return;
	}

	if (mode_pointer != mode_list[mode_num])
	{
		mode_list[mode_num]->resetMode();
	}

	mode_pointer = mode_list[mode_num];

	mode_pointer->updateMode();
}

void StateTracker::parseAuxChannels()
{
	int16_t buffer = armed;
	auxParser(receiver->rx_channels[4], 2, &buffer);
	armed = (bool)buffer;

	buffer = mode_num;
	auxParser(receiver->rx_channels[5], 2, &buffer);
	mode_num = buffer;

#if HAL_STATE_TRACKER_USE_TRIM
	buffer = 0;
	auxParser(receiver->rx_channels[6], 2, &buffer);
	if (buffer)
	{
		StorageManager* sm = HAL::getInstance().getStorageManager();
		if (sm)
		{
			char buffer[70];
			snprintf(buffer, sizeof(buffer), "Trims roll:%d, yaw:%d, pitch:%d\n",
					-(int16_t(receiver->rx_channels[0]) - (HAL_SERVO_RANGE / 2)),
					-(int16_t(receiver->rx_channels[3]) - (HAL_SERVO_RANGE / 2)),
					-(int16_t(receiver->rx_channels[1]) - (HAL_SERVO_RANGE / 2)));
			sm->getLogManager()->print(buffer);
		}
	}
#endif
}

bool StateTracker::auxParser(int16_t value, int8_t segments, int16_t *out)
{
	if (segments == 2)
	{
		if(value < 950)
		{
			*out = 0;
			return true;
		}
		else if (value > 1050)
		{
			*out = 1;
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (segments == 3)
	{
		if(value < 616)
		{
			*out = 0;
			return true;
		}
		else if (1283 > value and value > 716)
		{
			*out = 1;
			return true;
		}
		else if (value > 1383)
		{
			*out = 2;
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;

}


