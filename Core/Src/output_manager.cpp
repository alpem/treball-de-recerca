/*
 * output_manager.cpp
 *
 *  Created on: Sep 16, 2021
 *      Author: alpemwarrior
 */


#include "output_manager.hpp"
#include "log_manager_wrapper.hpp"
#include "hal.hpp"

OutputManager::OutputManager() {};

void OutputManager::init(Servo **servo_list, uint8_t size)
{
	this->servo_list = servo_list;
	this->size = size;
	jobRegister(JOB_FREQ_500HZ);

	StorageManager* sm = HAL::getInstance().getStorageManager();

	trim_yaw = 0;
	trim_roll = 0;
	trim_pitch = 0;

	if (sm)
	{
		sm->getConfigManager()->getValue(OUTPUT_MANAGER_TRIM_YAW_KEY, &trim_yaw);
		sm->getConfigManager()->getValue(OUTPUT_MANAGER_TRIM_ROLL_KEY, &trim_roll);
		sm->getConfigManager()->getValue(OUTPUT_MANAGER_TRIM_PITCH_KEY, &trim_pitch);
	}

	writeLog("Initialized output manager");
}

void OutputManager::setFromUnitAndClamped(float value, uint8_t index)
{
	if (value > 0.99999999)
	{
		set(index, HAL_SERVO_RANGE);
		return;
	}
	if (value < -0.9999999)
	{
		set(index, 0);
		return;
	}
	set(index, ((value + 1.0) / 2.0) * HAL_SERVO_RANGE);
	return;
}

void OutputManager::set(uint8_t index, int16_t value)
{
	if (index == 0)
	{
		int16_t res = value + trim_roll;
		if (res > HAL_SERVO_RANGE) res = HAL_SERVO_RANGE;
		else if (res < 0) res = 0;
		out_values[0] = res;
		return;
	}
	if (index == 1)
	{
		int16_t res = value + trim_pitch;
		if (res > HAL_SERVO_RANGE) res = HAL_SERVO_RANGE;
		else if (res < 0) res = 0;
		out_values[1] = res;
		return;
	}
	if (index == 3)
	{
		int16_t res = value + trim_yaw;
		if (res > HAL_SERVO_RANGE) res = HAL_SERVO_RANGE;
		else if (res < 0) res = 0;
		out_values[3] = res;
		return;
	}
	out_values[index] = value;
	return;
}

uint16_t OutputManager::get(uint8_t index)
{
	return out_values[index];
}

void OutputManager::jobLoop()
{
	for (int i = 0; i < size; i++)
	{
		((servo_list)[i])->update(out_values[i]);
	}
}
