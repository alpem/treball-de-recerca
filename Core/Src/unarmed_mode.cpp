/*
 * unarmed_mode.cpp
 *
 *  Created on: Sep 21, 2021
 *      Author: alpemwarrior
 */

#include "modes/unarmed_mode.hpp"

void UnarmedMode::updateMode()
{
	out->set(0, rx->rx_channels[0]);
	out->set(1, rx->rx_channels[1]);
	out->set(2, 0);
	out->set(3, rx->rx_channels[3]);
}

void UnarmedMode::resetMode()
{

}


