/*
 * quaternion.cpp
 *
 *  Created on: 10 d’ag. 2021
 *      Author: alpemwarrior
 */

#include "utils/quaternion.hpp"
#include <cmath>

Quaternion operator* (const Quaternion& x, const Quaternion& y)
{
	Quaternion quaternion;
	quaternion.a = x.a * y.a - x.b * y.b - x.c * y.c - x.d * y.d;
	quaternion.b = x.a * y.b + x.b * y.a + x.c * y.d - x.d * y.c;
	quaternion.c = x.a * y.c - x.b * y.d + x.c * y.a + x.d * y.b;
	quaternion.d = x.a * y.d + x.b * y.c - x.c * y.b + x.d * y.a;
	return quaternion;
}

/* Taken from https://github.com/YclepticStudios/gmath.
 * Licensed under MIT (See licenses/MIT.txt) */
void slerp(Quaternion q_o, Quaternion q_f, float t, Quaternion* q_out)
{
	if (t > 0.999999999f) *q_out = q_f;
	if (t < 0.000000001f) *q_out = q_o;

	float n1;
	float n2;
	float n3 = q_o.a * q_f.a + q_o.b * q_f.b + q_o.c * q_f.c + q_o.d * q_f.d;
	bool flag = false;
	if (n3 < 0)
	{
		flag = true;
		n3 = -n3;
	}
	if (n3 > 0.999999)
	{
		n2 = 1 - t;
		n1 = flag ? -t : t;
	}
	else
	{
		double n4 = acos(n3);
		double n5 = 1 / sin(n4);
		n2 = sin((1 - t) * n4) * n5;
		n1 = flag ? -sin(t * n4) * n5 : sin(t * n4) * n5;
	}
	q_out->b = (n2 * q_o.b) + (n1 * q_f.b);
	q_out->c = (n2 * q_o.c) + (n1 * q_f.c);
	q_out->d = (n2 * q_o.d) + (n1 * q_f.d);
	q_out->a = (n2 * q_o.a) + (n1 * q_f.a);
	q_out->normalize();
}

void Quaternion::scale(float s)
{
	a *= s;
	b *= s;
	c *= s;
	d *= s;
	return;
}

Quaternion::Quaternion()
{
	a = 1.0;
	b = 0.0;
	c = 0.0;
	d = 0.0;
	return;
}

Quaternion::Quaternion(float v1, float v2, float v3, float rot)
{
	float sin_half_angle = sin(rot / 2.0);
	a = cos(rot / 2.0);
	b = v1 * sin_half_angle;
	c = v2 * sin_half_angle;
	d = v3 * sin_half_angle;
	return;
}

void Quaternion::transformVectorInv(float vin_x, float vin_y, float vin_z, float* vout_x, float* vout_y, float* vout_z)
{
	Quaternion inverted;
	inverted.a = a;
	inverted.b = -b;
	inverted.c = -c;
	inverted.d = -d;

	Quaternion vector;
	vector.a = 0.0;
	vector.b = vin_x;
	vector.c = vin_y;
	vector.d = vin_z;

	Quaternion out = ((*this) * vector) * inverted;

	*vout_x = out.b;
	*vout_y = out.c;
	*vout_z = out.d;
}

void Quaternion::normalize()
{
	float norm = sqrt(pow(a, 2.0) + pow(b, 2.0) + pow(c, 2.0) + pow(d, 2.0));
	a /= norm;
	b /= norm;
	c /= norm;
	d /= norm;
	return;
}
