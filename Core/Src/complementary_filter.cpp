/*
 * complementary_filter.cpp
 *
 *  Created on: Sep 7, 2021
 *      Author: alpemwarrior
 */

#include <limits>
#include <cmath>

#include "utils/complementary_filter.hpp"
#include "log_manager_wrapper.hpp"

#define M_PI            3.14159265358979323846

#define ACCEL_MAX_ANGLE 1.6 * M_PI

//Local declination in degrees
#define MAG_LOCAL_DECLINATION 1.5

void ComplementaryFilter::init(float period, float tau)
{
	this->alpha = tau / (tau + period);
	initial = true;
	writeLog("Complementary filter initialized");
}

/* Performs the necessary calculations for estimating attitude. Note that this function may contain optimizations not described in the document */
void ComplementaryFilter::loop(float* gyro_x,  float* gyro_y,  float* gyro_z, float* accel_x, float* accel_y, float* accel_z, float* mag_x, float* mag_y, float* mag_z)
{
		if (initial)
		{
			initial = !setOrientationFromAccelandMag(accel_x, accel_y, accel_z, mag_x, mag_y, mag_z);
			last_read = HAL_GetTick();
			return;
		}

		float used_alpha = alpha;

		float accel_vector_module = sqrt(pow(*accel_x, 2.0) + pow(*accel_y, 2.0) + pow(*accel_z, 2.0));

		/* If the acceleration vector module is out of the tolerances, give full priority to the gyrosocope. This is a mitigation
		 * for high dynamic accelerations, which can distort the reading. Also avoids division by zero*/
		if (accel_vector_module < ACCEL_ORIENTATION_MIN_GRAVITY or accel_vector_module > ACCEL_ORIENTATION_MAX_GRAVITY)
		{
			used_alpha = 1.0;
		}

		/* Normalize acceleration vector */
		float accel_vector_x = *accel_x / accel_vector_module;
		float accel_vector_y = *accel_y / accel_vector_module;
		float accel_vector_z = *accel_z / accel_vector_module;

		/* Calculate angle between acceleration vector and z axis. The acceleration vector represents the Z-axis of the fixed reference
		 * system, thus the angle of rotation is the opposite. */
		float angle = -acos(accel_vector_z);

		/* When the angle approaches 180 x and y tend to zero, meaning that any noise can make the euler axis spin.
		 * In this case, give full priority to the gyroscope. */
		if (std::abs(angle) > ACCEL_MAX_ANGLE) used_alpha = 1.0;

		/* The Euler angle of the accelerometer data lives in the xy plane. */
		float euler_axis_normalizer = sqrt(pow(accel_vector_x, 2.0) + pow(accel_vector_y, 2.0));

		/* Avoid division by zero */
		if (std::abs(euler_axis_normalizer) <= std::numeric_limits<float>::epsilon()) return;

		/* Rotate horizontal components pi/2 degrees counterclockwise (viewed from the top) and normalize to get Euler angle. */
		float euler_axis_x = -accel_vector_y / euler_axis_normalizer;
		float euler_axis_y =  accel_vector_x / euler_axis_normalizer;

		/* Construct xy orientation */
		Quaternion xy_orienation =  Quaternion(euler_axis_x, euler_axis_y, 0.0, angle);

		/* Variables to hold the transformed mag readings */
		float mag_transformed_x = 0.0;
		float mag_transformed_y = 0.0;
		float mag_transformed_z = 0.0;

		/* Transform magnetometer vectors into fixed frame of reference to extract horizontal components */
		xy_orienation.transformVectorInv(*mag_x, *mag_y, *mag_z, &mag_transformed_x, &mag_transformed_y, &mag_transformed_z);

		/* Avoid division by zero */
		if (std::abs(mag_transformed_x) <= std::numeric_limits<float>::epsilon()) return;

		/* The azimuth is usually represented as a clockwise rotation, but this isn't useful here, as a right-hand system has a positive rotation on the counter-clockwise direction.
		 * The magnetometer vector represents the X-axis of the fixed reference system, thus the angle of rotation is the opposite. It is also important to remember that magnetometers
		 * measure magnetic fields perpendicular to the axis. */
		float azimuth = (-std::atan2(mag_transformed_y, mag_transformed_x)) + (M_PI / 2) - ((float)MAG_LOCAL_DECLINATION * 2 * M_PI / 360.0);

		Quaternion magaccel_orienation = Quaternion(0.0, 0.0, 1.0, azimuth)  * xy_orienation;

		float time_delta = (float)(HAL_GetTick() - last_read) / 1000.0;
		last_read = HAL_GetTick();

		float angular_speed_module = sqrt(pow(*gyro_x, 2.0) + pow(*gyro_y, 2.0) + pow(*gyro_z, 2.0));

		/* Avoid division by zero */
		if (std::abs(angular_speed_module) <= std::numeric_limits<float>::epsilon()) return;

		/* Construct rotation quaternion */
		Quaternion euler_axis;
		euler_axis.a = 0;
		euler_axis.b = *gyro_x / angular_speed_module;
		euler_axis.c = *gyro_y / angular_speed_module;
		euler_axis.d = *gyro_z / angular_speed_module;
		Quaternion gyro_orienation = Quaternion(euler_axis.b, euler_axis.c, euler_axis.d, angular_speed_module * time_delta);

		/* Apply rotation as intrinsic */
		gyro_orienation = orientation * gyro_orienation;

		/* Set new orientation for complementary filter */
		slerp(magaccel_orienation, gyro_orienation, used_alpha, &orientation);

		return;
}

/* Set initial starting point */
bool ComplementaryFilter::setOrientationFromAccelandMag(float* accel_x, float* accel_y, float* accel_z, float* mag_x,   float* mag_y,   float* mag_z)
{

	float accel_vector_module = sqrt(pow(*accel_x, 2.0) + pow(*accel_y, 2.0) + pow(*accel_z, 2.0));

	/* If the acceleration vector module is out of the tolerances, give full priority to the gyrosocope. This is a mitigation
	 * for high dynamic accelerations, which can distort the reading. Also avoids division by zero*/
	if (accel_vector_module < ACCEL_ORIENTATION_MIN_GRAVITY or accel_vector_module > ACCEL_ORIENTATION_MAX_GRAVITY)
	{
		return false;
	}

	/* Normalize acceleration vector */
	float accel_vector_x = *accel_x / accel_vector_module;
	float accel_vector_y = *accel_y / accel_vector_module;
	float accel_vector_z = *accel_z / accel_vector_module;

	/* Calculate angle between acceleration vector and z axis. The acceleration vector represents the Z-axis of the fixed reference
	 * system, thus the angle of rotation is the opposite. */
	float angle = -acos(accel_vector_z);

	/* When the angle approaches 180 x and y tend to zero, meaning that any noise can make the euler axis spin.
	 * In this case, give full priority to the gyroscope. */
	if (std::abs(angle) > ACCEL_MAX_ANGLE) return false;

	/* The Euler angle of the accelerometer data lives in the xy plane. */
	float euler_axis_normalizer = sqrt(pow(accel_vector_x, 2.0) + pow(accel_vector_y, 2.0));

	/* Avoid division by zero */
	if (std::abs(euler_axis_normalizer) <= std::numeric_limits<float>::epsilon()) return false;

	/* Rotate horizontal components pi/2 degrees counterclockwise (viewed from the top) and normalize to get Euler angle. */
	float euler_axis_x = -accel_vector_y / euler_axis_normalizer;
	float euler_axis_y =  accel_vector_x / euler_axis_normalizer;

	/* Construct xy orientation */
	Quaternion xy_orienation =  Quaternion(euler_axis_x, euler_axis_y, 0.0, angle);

	/* Variables to hold the transformed mag readings */
	float mag_transformed_x = 0.0;
	float mag_transformed_y = 0.0;
	float mag_transformed_z = 0.0;

	/* Transform magnetometer vectors into fixed frame of reference to extract horizontal components */
	xy_orienation.transformVectorInv(*mag_x, *mag_y, *mag_z, &mag_transformed_x, &mag_transformed_y, &mag_transformed_z);

	/* Avoid division by zero */
	if (std::abs(mag_transformed_x) <= std::numeric_limits<float>::epsilon()) return false;

	/* The azimuth is usually represented as a clockwise rotation, but this isn't useful here, as a right-hand system has a positive rotation on the counter-clockwise direction.
	 * The magnetometer vector represents the X-axis of the fixed reference system, thus the angle of rotation is the opposite. It is also important to remember that magnetometers
	 * measure magnetic fields perpendicular to the axis. */
	float azimuth = (-atan2(mag_transformed_y, mag_transformed_x)) + (M_PI / 2) - ((float)MAG_LOCAL_DECLINATION * 2 * M_PI / 360.0);

	/* Combine the two rotations. Note that because this is done as an extrinsic rotation, the multiplication is inverted. */
	orientation =  Quaternion(0, 0, 1, azimuth)  * xy_orienation;

	return true;
}


