/*
 * stabilized_mode.cpp
 *
 *  Created on: Oct 6, 2021
 *      Author: alpemwarrior
 */

#include "modes/stabilized_mode.hpp"
#include "hal.hpp"

#define M_PI 3.14159265358979323846
#define DOUBLE_M_PI (2.0 * M_PI)

StabilizedMode::StabilizedMode()
{
	StorageManager* sm = HAL::getInstance().getStorageManager();

	float p, i, d;

	p = 0.0;
	i = 0.0;
	d = 0.0;
	if (sm)
	{
		sm->getConfigManager()->getValue(PID_CONTROLLER_ANGLE_XP_KEY, &p);
		sm->getConfigManager()->getValue(PID_CONTROLLER_ANGLE_XI_KEY, &i);
		sm->getConfigManager()->getValue(PID_CONTROLLER_ANGLE_XD_KEY, &d);
	}
	pid_x.setParameters(p, i, d);

	p = 0.0;
	i = 0.0;
	d = 0.0;
	if (sm)
	{
			sm->getConfigManager()->getValue(PID_CONTROLLER_ANGLE_YP_KEY, &p);
			sm->getConfigManager()->getValue(PID_CONTROLLER_ANGLE_YI_KEY, &i);
			sm->getConfigManager()->getValue(PID_CONTROLLER_ANGLE_YD_KEY, &d);
	}
	pid_y.setParameters(p, i, d);

	p = 0.0;
	i = 0.0;
	d = 0.0;
	if (sm)
	{
			sm->getConfigManager()->getValue(PID_CONTROLLER_ANGLE_ZP_KEY, &p);
			sm->getConfigManager()->getValue(PID_CONTROLLER_ANGLE_ZI_KEY, &i);
			sm->getConfigManager()->getValue(PID_CONTROLLER_ANGLE_ZD_KEY, &d);
	}
	pid_z.setParameters(p, i, d);

	pid_x.setSetpoint(0.0);
	pid_y.setSetpoint(0.0);
	pid_z.setSetpoint(0.0);

	mp = HAL::getInstance().getMotionProcessor();
}

void StabilizedMode::resetMode()
{

	pid_x.reset();
	pid_y.reset();
	pid_z.reset();

	pid_z.setSetpoint(mp->angular_position_ea.z);
}

void StabilizedMode::updateMode()
{
	out->setFromUnitAndClamped(pid_x.loop(mp->angular_position_ea.x), 1);
	out->setFromUnitAndClamped(pid_y.loop(mp->angular_position_ea.y), 0);
    out->setFromUnitAndClamped(pid_z.loop(mp->angular_position_ea.z), 3);
    out->set(2, 0);
}
